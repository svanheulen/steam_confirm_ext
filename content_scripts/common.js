// Convert ArrayBuffer to a Base64 string
function base64_encode(data) {
    var data_bytes = new Uint8Array(data);
    var data_string = new String();
    for (i = 0; i < data_bytes.length; i++)
        data_string += String.fromCharCode(data_bytes[i]);
    return btoa(data_string);
}

// Convert a Base64 string to a Uint8Array
function base64_decode(data_base64) {
    var data_string = atob(data_base64);
    var data_bytes = new Uint8Array(data_string.length);
    for (i = 0; i < data_string.length; i++)
        data_bytes[i] = data_string.charCodeAt(i);
    return data_bytes;
}

// Format the data that will be signed with the Steam Mobile identity secret
function make_confirmation_data(timestamp, tag) {
    var encoder = new TextEncoder();
    var tag_bytes = encoder.encode(tag);
    var data = new ArrayBuffer(8 + tag_bytes.length);
    var data_view = new DataView(data);
    data_view.setUint32(0, Math.floor(timestamp / 0x100000000), false);
    data_view.setUint32(4, timestamp & 0xffffffff, false);
    var data_bytes = new Uint8Array(data);
    data_bytes.set(tag_bytes, 8);
    return data;
}

