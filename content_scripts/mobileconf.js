var identity_secret = null;

function GetValueFromLocalURL(url, timeout, success, error, fatal) {
    var timestamp = Math.floor(Date.now() / 1000);
    // Get the tag from the "steammobile://..." URL
    var steammobile_url = new URL(url);
    var tag = steammobile_url.searchParams.get("arg1");
    // Sign the confirmation data with the user's Steam Mobile identity secret
    var data = make_confirmation_data(timestamp, tag);
    window.crypto.subtle.sign({name: "HMAC"}, identity_secret, data).then(function (signature) {
        // Update the GET parameters for the page
        var confirmations_page_url = new URL(window.location.toString());
        confirmations_page_url.searchParams.set("k", base64_encode(signature));
        confirmations_page_url.searchParams.set("t", timestamp);
        confirmations_page_url.searchParams.set("tag", tag);
        // Pass the GET parameters to the success function
        success(confirmations_page_url.searchParams.toString());
    });
}

function override_steammobile_requests() {
    // Get the user's Steam ID from the page script
    steam_id = window.wrappedJSObject.g_steamID;
    if (!steam_id)
        return;
    browser.storage.local.get(steam_id).then(function (settings) {
        // Import the user's Steam Mobile identity secret as an HMAC key
        var stored_identity_secret = base64_decode(settings[steam_id]);
        return window.crypto.subtle.importKey("raw", stored_identity_secret, {name: "HMAC", hash: {name: "SHA-1"}}, false, ["sign"]);
    }).then(function (key) {
        // Store the user's HMAC key
        identity_secret = key;
        // Override the page script function that provides the GET parameters for requests
        window.wrappedJSObject.GetValueFromLocalURL = exportFunction(GetValueFromLocalURL, window);
    });
}

override_steammobile_requests();

