var steam_id = null;
var identity_secret = null;
var device_id = null;

function open_confirmations_page() {
    var timestamp = Math.floor(Date.now() / 1000);
    // Sign the confirmation data with the user's Steam Mobile identity secret
    var data = make_confirmation_data(timestamp, "conf");
    window.crypto.subtle.sign({name: "HMAC"}, identity_secret, data).then(function (signature) {
        // Build the confirmations page URL
        var confirmations_page_url = new URL("https://steamcommunity.com/mobileconf/conf");
        confirmations_page_url.searchParams.set("p", device_id);
        confirmations_page_url.searchParams.set("a", steam_id);
        confirmations_page_url.searchParams.set("k", base64_encode(signature));
        confirmations_page_url.searchParams.set("t", timestamp);
        confirmations_page_url.searchParams.set("m", "android");
        confirmations_page_url.searchParams.set("tag", "conf");
        // Redirect to the confirmations page
        window.open(confirmations_page_url, "_self");
    });
}

function add_confirmations_link() {
    // Get the user's Steam ID from the page script
    steam_id = window.wrappedJSObject.g_steamID;
    if (!steam_id)
        return;
    browser.storage.local.get(steam_id).then(function (settings) {
        // Import the user's Steam Mobile identity secret as an HMAC key
        var stored_identity_secret = base64_decode(settings[steam_id]);
        return window.crypto.subtle.importKey("raw", stored_identity_secret, {name: "HMAC", hash: {name: "SHA-1"}}, false, ["sign"]);
    }).then(function (key) {
        // Store the user's HMAC key
        identity_secret = key;
        // Hash the user's Steam ID for making the device ID
        var encoder = new TextEncoder();
        var steam_id_bytes = encoder.encode(steam_id);
        return window.crypto.subtle.digest("SHA-1", steam_id_bytes);
    }).then(function (hash) {
        // Format the device ID
        var h = Array.prototype.map.call(new Uint8Array(hash), function (x) {return x.toString(16).padStart(2, "0");}).join("");
        device_id = "android:" + [h.slice(0, 8), h.slice(8, 12), h.slice(12, 16), h.slice(16, 20), h.slice(20, 32)].join("-");
        // Add a "Confirmations..." button to the page
        var content = document.createElement("div");
        content.setAttribute("class", "rightcol_controls_content");
        var div = content.appendChild(document.createElement("div"));
        div.setAttribute("class", "btn_darkblue_white_innerfade btn_medium new_trade_offer_btn");
        div.addEventListener("click", open_confirmations_page);
        div.appendChild(document.createElement("span")).appendChild(document.createTextNode("Confirmations..."));
        var rule = document.createElement("div");
        rule.setAttribute("class", "rightcol_controls_rule");
        var rightcol_controls = document.getElementsByClassName("rightcol_controls")[0];
        rightcol_controls.insertBefore(content, rightcol_controls.childNodes[2]);
        rightcol_controls.insertBefore(rule, rightcol_controls.childNodes[2]);
    });
}

add_confirmations_link();

