function load_identity_secrets() {
    var table = document.getElementById("identity_secrets_table");
    while (table.firstChild)
        table.removeChild(table.firstChild);
    browser.storage.local.get().then(function (settings) {
        for (const steam_id in settings) {
            var row = table.appendChild(document.createElement("tr"));
            row.appendChild(document.createElement("td")).appendChild(document.createTextNode(steam_id));
            row.appendChild(document.createElement("td")).appendChild(document.createTextNode(settings[steam_id]));
            var remove_button = row.appendChild(document.createElement("td")).appendChild(document.createElement("button"));
            remove_button.setAttribute("type", "button");
            remove_button.appendChild(document.createTextNode("Remove"));
            remove_button.addEventListener('click', function () {
                remove_identity_secret(steam_id);
            });
        }
    });
}

function add_identity_secret() {
    var steam_id_input = document.getElementById("steam_id_input");
    var identity_secret_input = document.getElementById("identity_secret_input");
    browser.storage.local.set({[steam_id_input.value]: identity_secret_input.value}).then(load_identity_secrets);
    steam_id_input.value = "";
    identity_secret_input.value = "";
}

function remove_identity_secret(steam_id) {
    browser.storage.local.remove(steam_id).then(load_identity_secrets);
}

document.addEventListener('DOMContentLoaded', load_identity_secrets);
document.getElementById("add_button").addEventListener('click', add_identity_secret);

